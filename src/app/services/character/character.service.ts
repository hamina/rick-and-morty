import { Injectable } from '@angular/core';
import { Character } from 'src/app/domain/main/character';
import { LocalService } from '../local/local.service';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  private url;
  private localNameVarible="favorite";
  /// Este servicio sirve para las operaciones que se hacen con el personaje por si se necesitara reusar en algun otro componente
  constructor(private localService:LocalService) {
    this.url = "https://rickandmortyapi.com/api";
  }

  //reduzco los capitulos a una cadena de string para poderlos mostrar
  getCaps(character: Character) {
    const initial: string = "";
    return character.episode?.reduce((prev, current) =>
      prev + ", " + current.replace(this.url + "/episode/", ""), initial
    )

  }
  //obtengo el último episodio donde apareció
  getLastEpisode(character: Character) {
    //inicializó dado el caso que no haya 
    let lasEpisode = 0;
    //lo seteo los episodios del personaje
    let episodes = character.episode;
    if (episodes != undefined) { //siempre y cuando no sean indefinidos
      //me devolvera el número del cap donde apareció
      return episodes[episodes.length - 1].replace(this.url + "/episode/", "")
    }
    return lasEpisode;
  }
  //obtengo el nombre de la locación del personaje
  getLocation(character: Character) {
    return character.location ? character.location.name : "";
  }
  //obtengo la url de la locación según la ruta preestablecida
  getLocationUrl(character: Character) {
    return character.location ? character.location.url?.replace(this.url, "") : "";
  }
  //esta función es para saber si el personaje está en favoritos o no
  findFavorite(character:Character){
    let id=character.id;
    //traigo la lista del local service
    let favoriteCharacterList:Array<Character>=this.localService.getItem(this.localNameVarible);
    //si la lista no existe entonces
    if(!favoriteCharacterList) return {found:false,position:0};
    //busco si el objeto está
    for (const [index,value] of favoriteCharacterList.entries()) {
      if(value.id==id) return {found:true,position:index}
    }
    //No está el objeto entonces no está en favoritos
    return {found:false,position:0};
  }
  setFavorite(character:Character){
    //sino lo encuentra, es una pequeña validación por silas
    if(!this.findFavorite(character).found){
      let favoriteCharacterList:Array<Character>=this.localService.getItem(this.localNameVarible);
      //si no existe la lista la creo
      if(!favoriteCharacterList) favoriteCharacterList=[];
      //pongo el elemento en la lista
      favoriteCharacterList.push(character);
      //le digo al local service que lo guarde
      this.localService.setItem(this.localNameVarible,favoriteCharacterList)

    } 
    
  }
  deleteFavorite(character:Character){
    //sino lo encuentra, es una pequeña validación por silas
    let finder=this.findFavorite(character);
    if(finder.found){
      let favoriteCharacterList:Array<Character>=this.localService.getItem(this.localNameVarible);
      //si no existe la lista la creo
      if(!favoriteCharacterList) favoriteCharacterList=[];
      //elimino el objeto
      favoriteCharacterList.splice(finder.position,1)
      //le digo al local service que lo guarde
      this.localService.setItem(this.localNameVarible,favoriteCharacterList)
    } 
  }
  getFavorites(){
    let favoriteCharacterList:Array<Character>=this.localService.getItem(this.localNameVarible);
    return favoriteCharacterList;
  }
}
