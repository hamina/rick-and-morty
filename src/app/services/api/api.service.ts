import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Character } from 'src/app/domain/main/character';
import { Location } from 'src/app/domain/main/location';
import { ResponseCharacter } from 'src/app/domain/responses/response-character';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private url;
  constructor(private http: HttpClient) {
    this.url = "https://rickandmortyapi.com/api";
  }
 
  getCharacter(page: number,) {
    return this.http.get<ResponseCharacter>(this.url + "/character/?page=" + page)
  }
  getCharacterDetail(id: number) {
    return this.http.get<Character>(this.url + "/character/" + id)
  }
  getLocation(id: number) {
    return this.http.get<Location>(this.url + "/location/" + id)
  }
}
