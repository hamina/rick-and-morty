import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalService {

  constructor() { }
  getItem(nombre: string) {
    var obj = localStorage.getItem(nombre);
    if (obj) {
      return this.decode(obj);
    } else {
      return null;
    }

  }
  setItem(nombre: string, obj: any) {
    localStorage.setItem(nombre, this.code(obj));
  }
  removeItem(nombre: string) {
    localStorage.removeItem(nombre);
  }
  private code(objeto:any) {
    return btoa(JSON.stringify(objeto));
  }
  private decode(objeto:any) {
    return JSON.parse(atob(objeto));
  }
}
