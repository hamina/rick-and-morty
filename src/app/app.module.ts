import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CaracterListComponent } from './components/caracter-list/caracter-list.component';
import { CaracterDetailComponent } from './components/caracter-detail/caracter-detail.component';
import { CaracterLocationComponent } from './components/caracter-location/caracter-location.component';
import { CaracterFavoriteComponent } from './components/caracter-favorite/caracter-favorite.component';
import { CaracterCardComponent } from './components/caracter-card/caracter-card.component';
import { HttpClientModule } from '@angular/common/http';
import { SwiperModule } from 'swiper/angular';
@NgModule({
  declarations: [
    AppComponent,
    CaracterListComponent,
    CaracterDetailComponent,
    CaracterLocationComponent,
    CaracterFavoriteComponent,
    CaracterCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SwiperModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
