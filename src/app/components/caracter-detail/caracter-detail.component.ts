import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Character } from 'src/app/domain/main/character';
import { ApiService } from 'src/app/services/api/api.service';
import { CharacterService } from 'src/app/services/character/character.service';

@Component({
  selector: 'app-caracter-detail',
  templateUrl: './caracter-detail.component.html',
  styleUrls: ['./caracter-detail.component.scss']
})
export class CaracterDetailComponent implements OnInit {

  private id: number;
  public character: Character;

  constructor(private route: ActivatedRoute, private api: ApiService,
    public characterService:CharacterService) {
    this.id = parseInt(this.route.snapshot.params["id"]);
    this.character = {};
  }

  ngOnInit(): void {
    this.api.getCharacterDetail(this.id).subscribe(
      response => {
        this.character = response;
        let finder=this.characterService.findFavorite(this.character);
        this.character.favorite=finder.found;
      }
    )
  }
  setFavorite(isFavorite:boolean){
    this.character.favorite=isFavorite;
    isFavorite ?
      this.characterService.setFavorite(this.character):
      this.characterService.deleteFavorite(this.character);
  }
}
