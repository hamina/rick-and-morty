import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from 'src/app/domain/main/location';
import { ApiService } from 'src/app/services/api/api.service';
import { SwiperComponent } from "swiper/angular";

// import Swiper core and required modules
import SwiperCore, { EffectCoverflow, Navigation } from "swiper";

// install Swiper modules
SwiperCore.use([EffectCoverflow, Navigation]);

@Component({
  selector: 'app-caracter-location',
  templateUrl: './caracter-location.component.html',
  styleUrls: ['./caracter-location.component.scss']
})
export class CaracterLocationComponent implements OnInit {

  private id:number;
  public location:Location;

  constructor(private route: ActivatedRoute,private api:ApiService) {
    this.id=parseInt(this.route.snapshot.params["id"]);
    this.location={};
   }

  ngOnInit(): void {
    this.api.getLocation(this.id).subscribe(
      response=>{
        this.location=response;
        this.location.residents=this.location.residents?.map(resident=>{
          return resident.split("/character/")[1];
        })
      }
    )
  }

  getInitialSlide(){
    return this.location.residents? this.location.residents.length/2:0;
  }
}
