import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaracterLocationComponent } from './caracter-location.component';

describe('CaracterLocationComponent', () => {
  let component: CaracterLocationComponent;
  let fixture: ComponentFixture<CaracterLocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaracterLocationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CaracterLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
