import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaracterListComponent } from './caracter-list.component';

describe('CaracterListComponent', () => {
  let component: CaracterListComponent;
  let fixture: ComponentFixture<CaracterListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaracterListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CaracterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
