import { Component, OnInit } from '@angular/core';
import { Character } from 'src/app/domain/main/character';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-caracter-list',
  templateUrl: './caracter-list.component.html',
  styleUrls: ['./caracter-list.component.scss']
})
export class CaracterListComponent implements OnInit {
  private page:number;
  public characterList:Array<Character>;
  constructor(private api:ApiService) {
    this.page=1;
    this.characterList=[]
  }

  ngOnInit(): void {
    this.cargarPersonajes(this.page)
  }
  //La función del botón
  cargarMas(){
    //agrego página
    this.page++;
    //sumo personajes
    this.cargarPersonajes(this.page)
  }
  //función para cargar los personajes
  cargarPersonajes(page:number){
    this.api.getCharacter(page).subscribe(response=>{
      this.characterList.push(...response.results);
    })
  }
}
