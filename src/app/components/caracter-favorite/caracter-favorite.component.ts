import { Component, OnInit } from '@angular/core';
import { Character } from 'src/app/domain/main/character';
import { CharacterService } from 'src/app/services/character/character.service';

@Component({
  selector: 'app-caracter-favorite',
  templateUrl: './caracter-favorite.component.html',
  styleUrls: ['./caracter-favorite.component.scss']
})
export class CaracterFavoriteComponent implements OnInit {

  public characterList:Array<Character>;
  constructor(private characterService:CharacterService) {
    this.characterList=this.characterService.getFavorites();
   }

  ngOnInit(): void {
  }

}
