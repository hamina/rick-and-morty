import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaracterFavoriteComponent } from './caracter-favorite.component';

describe('CaracterFavoriteComponent', () => {
  let component: CaracterFavoriteComponent;
  let fixture: ComponentFixture<CaracterFavoriteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaracterFavoriteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CaracterFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
