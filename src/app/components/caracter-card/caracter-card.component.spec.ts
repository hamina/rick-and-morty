import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaracterCardComponent } from './caracter-card.component';

describe('CaracterCardComponent', () => {
  let component: CaracterCardComponent;
  let fixture: ComponentFixture<CaracterCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaracterCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CaracterCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
