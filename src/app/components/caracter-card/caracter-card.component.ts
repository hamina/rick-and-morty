import { Component, Input, OnInit } from '@angular/core';
import { Character } from 'src/app/domain/main/character';
import { CharacterService } from 'src/app/services/character/character.service';


@Component({
  selector: 'app-caracter-card',
  templateUrl: './caracter-card.component.html',
  styleUrls: ['./caracter-card.component.scss']
})
export class CaracterCardComponent implements OnInit {

  @Input() character: Character;

  constructor(public characterService:CharacterService) {
    this.character = {}
  }

  ngOnInit(): void {

  }

}
