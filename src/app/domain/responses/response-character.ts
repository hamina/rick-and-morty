import { Character } from "../main/character";
import { Info } from "../main/info";

export interface ResponseCharacter {
    info:Info,
    results:Array<Character>
}
