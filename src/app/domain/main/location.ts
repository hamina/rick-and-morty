export interface Location {
    created?:string,
    name?:string,
    url?:string,
    dimension?:string,
    residents?:Array<any>,
    id?:number,
    type?:string,
}
