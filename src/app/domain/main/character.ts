import { Location } from "./location";

export interface Character {
    image?: string,
    created? : string,
    status?:string,
    episode?: Array<string>,
    gender?: string,
    id?: number,
    location?: Location,
    name?: string,
    origin?: any,
    species?: string,
    type?: string,
    url?: string,
    favorite?:boolean
}
