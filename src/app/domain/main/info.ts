export class Info {
    public count: number
    public pages: number
    private next: string
    private prev: string
    constructor(
        count:number,
        pages:number,
        next:string,
        prev:string){
            this.count=count
            this.pages=pages;
            this.next=next;
            this.prev=prev;
    }
    getNextPageUrl(){
        return this.next;
    }
    getPrevPageUrl(){
        return this.prev;
    }
    
}
