import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CaracterDetailComponent } from './components/caracter-detail/caracter-detail.component';
import { CaracterFavoriteComponent } from './components/caracter-favorite/caracter-favorite.component';
import { CaracterListComponent } from './components/caracter-list/caracter-list.component';
import { CaracterLocationComponent } from './components/caracter-location/caracter-location.component';

const routes: Routes = [
  { path: "", component: CaracterListComponent},
  { path: "detail/:id", component: CaracterDetailComponent},
  { path: "location/:id", component: CaracterLocationComponent},
  { path: "favorites", component: CaracterFavoriteComponent},
  { path: "**", component: CaracterListComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
