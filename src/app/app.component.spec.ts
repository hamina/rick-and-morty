import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { CaracterCardComponent } from './components/caracter-card/caracter-card.component';
import { CaracterDetailComponent } from './components/caracter-detail/caracter-detail.component';
import { CaracterFavoriteComponent } from './components/caracter-favorite/caracter-favorite.component';
import { CaracterListComponent } from './components/caracter-list/caracter-list.component';
import { CaracterLocationComponent } from './components/caracter-location/caracter-location.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        CaracterCardComponent,
        CaracterFavoriteComponent,
        CaracterDetailComponent,
        CaracterListComponent,
        CaracterLocationComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'pruebaEnEquipo'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('pruebaEnEquipo');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('pruebaEnEquipo app is running!');
  });
});
